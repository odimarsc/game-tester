package com.itau.front_tester.controllers;



import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.itau.front_tester.models.GameTest;

@CrossOrigin
@RestController
public class GameTestController {

	private GameTest game;
	
	@RequestMapping("/game/start/{nomeJogador}")
	public GameTest getGameStart (@PathVariable("nomeJogador") String nomeJogador ) {
		System.out.println("/game/start/" + nomeJogador);
		game = new GameTest();
		return game;
	}
	
	@RequestMapping("/game/start/perguntas/{idJogo}")
	public GameTest getGameTest(@PathVariable("idJogo") String idJogo ) {
		System.out.println("/game/start/perguntas/" + idJogo);
		game.runGameTest();
		System.out.println("getQtdeAcertos: " + game.getQtdeAcertos());		
		return game;
	}	
	
    @RequestMapping(method=RequestMethod.POST, path="/game/start/respostas/{idJogo}")
    public ResponseEntity<?> postResposta(@RequestBody GameTest gamePosted) {
        
    	System.out.println("Resposta jogador: " + gamePosted.listaPerguntas.get(0).getRespostaUsuario() + "   Certa: " + gamePosted.listaPerguntas.get(0).getAnswer());
    	
    	if (gamePosted.listaPerguntas.get(0).getRespostaUsuario() == game.listaPerguntas.get(0).getAnswer()) {
    		this.game.setQtdeAcertos(this.game.getQtdeAcertos()+1);
    	}
    	return ResponseEntity.ok(gamePosted);
    }

	
//	@RequestMapping("/game")
//	public ResponseEntity<?> getGameTest() {
//
//		game.preencheFake();
//		
//		if(game.qtdePerguntas < 1) {
//			return ResponseEntity.status(404).build();
////			return ResponseEntity.notFound().build();
//		}
//		return ResponseEntity.ok(game);
//	}
}
