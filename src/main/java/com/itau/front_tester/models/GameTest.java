package com.itau.front_tester.models;

import java.util.ArrayList;

public class GameTest {
	
	public int id;
	public String nomeUsuario;
	public int qtdePerguntas;
	public int qtdeAcertos;
	public ArrayList<Perguntas> listaPerguntas;

	public GameTest() {
		
		id = 1;
		nomeUsuario = "front_tester";
		qtdePerguntas = 0;
		qtdeAcertos=0;
		ArrayList<Perguntas> lista = new ArrayList<>();
	
		Perguntas p1 = new Perguntas();
		Perguntas p2 = new Perguntas();
		Perguntas p3 = new Perguntas();

		p1.id = 1; 
		p1.title = "What takes place on the weekend before the Super Bowl?";
		p1.category = "Football";
		p1.option1 = "The Pro Bowl";	
		p1.option2 = "Churchill Downs";
		p1.option3 = "Wrestlemania";
		p1.option4 = "The World Series";
		p1.answer = 1;
		p1.respostaUsuario= 0;
		lista.add(p1);
		qtdePerguntas++;
		
		p2.id = 2; 
		p2.title = "What song is sampled in Ice Cube's 'Check Yo Self'?";
		p2.category = "Genius Of Love";
		p2.option1 = "Hooked On A Feeling";	
		p2.option2 = "The Way We Were";
		p2.option3 = "Wrestlemania";
		p2.option4 = "The Message";
		p2.answer = 4;
		p2.respostaUsuario= 0;
		lista.add(p2);
		qtdePerguntas++;
		
		p3.id = 3; 
		p3.title = "What song is sampled in Ice Cube's 'Check Yo Self'?";
		p3.category = "Genius Of Love";
		p3.option1 = "Hooked On A Feeling";	
		p3.option2 = "The Way We Were";
		p3.option3 = "Wrestlemania";
		p3.option4 = "The Message";
		p3.answer = 4;
		p3.respostaUsuario= 0;
		lista.add(p3);
		qtdePerguntas++;
		
		listaPerguntas  = lista; 
	}
	
	public void runGameTest ()
	{
		// a nova rodada tira retira a 1a pergunta da lista
		if ( qtdePerguntas > 0 )
		{
		   listaPerguntas.remove(0);
		   qtdePerguntas--;
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public int getQtdePerguntas() {
		return qtdePerguntas;
	}
	public void setQtdePerguntas(int qtdePerguntas) {
		this.qtdePerguntas = qtdePerguntas;
	}
	public int getQtdeAcertos() {
		return qtdeAcertos;
	}
	public void setQtdeAcertos(int qtdeAcertos) {
		this.qtdeAcertos = qtdeAcertos;
	}
	public ArrayList<Perguntas> getListaPerguntas() {
		return listaPerguntas;
	}
	public void setListaPerguntas(ArrayList<Perguntas> listaPerguntas) {
		this.listaPerguntas = listaPerguntas;
	}
	
}