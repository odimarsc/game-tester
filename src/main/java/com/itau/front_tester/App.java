package com.itau.front_tester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.itau.front_tester.models.Pessoa;

@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
    	Pessoa p = new Pessoa("Odimar", 45);
    	System.out.println(p);
    	
        SpringApplication.run(App.class, args);
        
    }
}
